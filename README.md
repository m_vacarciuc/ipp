## oAuth

This is a prototype of the [oAuth](http://oauth.net/) authorization service. Run the server using `rails s`.

---
`register`

In order to register user must specify:

* email
* password
* app_id

---
`login`

In order to login user must specify:

* email
* password
* app_id

---
`last_login`

An endpoint for getting last login with a specific application. In order to get that, user must specify:

* app_id
* token
* email

---

#### Future

1. Check if all fields are present in request and ignore additional fields supplied in the request
2. Validate email field
3. Encrypt the password